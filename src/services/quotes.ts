import { Quotes } from '../data/quotes.interface';

export class QuotesService {
    private favoriteQuotes: Quotes[] = [];

    addQuoteToFavorites(quote: Quotes) {
        this.favoriteQuotes.push(quote);
    }

    //splice (index, jumlah yang mau dipop berapa, "value")
    removeQuoteFromFavorites(quote: Quotes) {
        this.favoriteQuotes.splice(this.favoriteQuotes.indexOf(quote),1);
    }
    getFavoriteQuotes() {}
    isFavorite(quote: Quotes) {
        if(this.favoriteQuotes.indexOf(quote) >= 0) {
            return 1;
        } else {
            return 0;
        }
    }
}