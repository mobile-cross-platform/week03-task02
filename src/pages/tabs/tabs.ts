import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FavoritesPage } from '../favorites/favorites';
import { LibraryPage } from '../library/library';

@IonicPage()
@Component({
  selector: 'page-tabs',
  template: `
    <ion-tabs>
      <ion-tab [root]="LibraryPage" tabTitle="Library" tabIcon="book"></ion-tab>
      <ion-tab [root]="FavoritesPage" tabTitle="Favorites" tabIcon="star"></ion-tab>
    </ion-tabs>
  `
})
export class TabsPage {

  LibraryPage = LibraryPage;
  FavoritesPage = FavoritesPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
